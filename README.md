# 高校科研管理系统

## 背景

### 这个项目是本人的毕业设计，本来想使用spring-cloud微服务的框架，但是学校的验收时间提前，只能先使用spring-boot做一个简单网页先应付一下学校-、- 所以偏向业务一点，但是还是会尽量把自己学到的技术都用上去，已达到练手的目的吧，后续会慢慢改善，此外前端打算采用vue-element来做前后分离

## 功能

### 人员管理

### 角色管理

### 权限管理

### 科研项目、学科竞赛 的增删改查、统计、申请和审核功能等

## 部署说明

### redis

### jdk1.8

### mysql

### idea(推荐)

## 启动说明

### 数据库建好，运行sql，启动redis，点击start，后台调试界面http://localhost:9999/gxkygl/swagger-ui.html （前端做好后会一并打包进来，暂时没有哦，请耐心等待） 默认账号密码 admin / admin

### 给几张目前完成的页面吧~~
 * 主页
 ![image](https://gitee.com/xiaoyaotonxue/JiYuspringbootDeGaoXiaoKeYanGuanLiXiTong/raw/master/src/main/resources/img/admin.jpg)
 * 用户管理
 ![image](https://gitee.com/xiaoyaotonxue/JiYuspringbootDeGaoXiaoKeYanGuanLiXiTong/raw/master/src/main/resources/img/user.jpg)
* 项目管理
![image](https://gitee.com/xiaoyaotonxue/JiYuspringbootDeGaoXiaoKeYanGuanLiXiTong/raw/master/src/main/resources/img/project.jpg)
* 权限管理
![image](https://gitee.com/xiaoyaotonxue/JiYuspringbootDeGaoXiaoKeYanGuanLiXiTong/raw/master/src/main/resources/img/permission.jpg)
* 数据字典
![image](https://gitee.com/xiaoyaotonxue/JiYuspringbootDeGaoXiaoKeYanGuanLiXiTong/raw/master/src/main/resources/img/dict.jpg)
* 其他的大同小异没有什么好贴得了  等完善后 会更新上前端代码

## 目标

### 1、完成毕业设计（业务跑通即可，真是水的不行）

### 2、后面会继续完善项目，打造成一个spring-cloud的demo系统

## 关于改善

### 有什么不到位的地方请大家多多指教（求前端大佬帮忙啊）

## 本人QQ 867160589


